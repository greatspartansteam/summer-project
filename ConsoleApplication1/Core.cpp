#include <stdio.h>
#include <locale.h>
#include <ctype.h>

void input_text(char *array_for_text);
char* find_the_most_frequent_word(char *array_of_words);
void convertToUpperCase(char *sPtr);
int main() {
	char *the_most_frequent_word = "";
	char array_for_text[1001];

	setlocale(LC_ALL,"rus");
	printf("���������� ������� �����:\n"); //����������� �� �����
	input_text(array_for_text);


	printf("����� ������������������ �����: %s\n", the_most_frequent_word); //����� ����������
	return 0;
}

/**
* function to get text from keyboard
*
* @param char *array_for_text - array which
* contains input text from user
* @return
*/
void input_text(char *array_for_text) {
    // getting text from stdin
    fgets(array_for_text, 1001, stdin);
}
/**
* function converts to Uppercase
*
* @param char *sPtr - array which
* contains raw
* @return
*/
void convertToUpperCase(char *sPtr)
{
	while (*sPtr != '\0') 
	{
		if (islower(*sPtr)) // ���� ������ � ������ �������� �� ������������� � �������
			*sPtr = toupper(*sPtr);
		*sPtr++; // ������� � ���������� �������
	}
}

char* find_the_most_frequent_word(char *array_for_text) {
	int words_count = 0; // ���������� ���� � ������
	char *array_of_wordsPtr[1000];  // ������ ���������� �� ������ �� �������
	array_of_wordsPtr[0] = strtok(array_for_text, " "); // ��������� ����������� ������ �����
														// ��������� ���������� ���� � ������
	while (array_of_wordsPtr[words_count] != NULL) {
		words_count++;
		array_of_wordsPtr[words_count] = strtok(NULL, " "); // ����� ��������� 
	}

	bool first_iter = true, go_to = false; // ���� ������ �������� � ������������ ������� 
	int max_num_1 = 0, max_num_2 = 0, max_word = 0, two_iter = 0;// ���������� �������� ������� �����, ���������� �������� ������� �����, �������� ����� �������������� �����, ������� ���� ��������
	int index1, index2;
	for (int i = 0; i < (words_count - 1); i++) { // ������� ����, � ������� �����
		two_iter++;
		for (int j = i + 1; j < words_count; j++) { // ������� ����, �� ������� �����
			if (strcmp(array_of_wordsPtr[i], array_of_wordsPtr[j]) == 0) // ���������� �����
				if (first_iter) // ���� ������ �������� 
				{
					max_num_1++;
					first_iter = false;
					index1 = i;
				}
				else {
					max_num_2++;
					index2 = i;
				}
		}
		if (two_iter == 2 || go_to) { // ���� ��� ���� ��� ��������
			if (max_num_1 >= max_num_2) { // ���������� ����������� ���������� 
				max_word = index1;
			}
			else {
				max_word = index2;
				max_num_1 = max_num_2;
			}
			two_iter = 0; // �������� ������� 
			go_to = true;
		}
		max_num_2 = 0;
	}

	return array_of_wordsPtr[max_word]; // ���������� �������� ������������� ����� 
}



















